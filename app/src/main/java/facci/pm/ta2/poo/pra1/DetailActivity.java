package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6

        String object_id = getIntent().getStringExtra("object_id");


        // FIN - CODE6

        DataQuery dataQuery = DataQuery.get("item");
        dataQuery.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {

                if (e == null){
                    TextView title = (TextView)findViewById(R.id.name);
                    TextView price = (TextView)findViewById(R.id.price);
                    ImageView thumbnail = (ImageView)findViewById(R.id.thumbnail);
                    TextView description = (TextView)findViewById(R.id.description);
                    description.setMovementMethod(LinkMovementMethod.getInstance());


                    title.setText((String) object.get("name"));
                    price.setText((String) object.get("price")+"\u0024");
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));
                    description.setText((String) object.get("description"));
                }else{
                    /////ERROR
                }
            }
        });


    }

}
